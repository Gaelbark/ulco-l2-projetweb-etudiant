<?php $leProduit=$params["info"] ?>
<div id="product">
    <div>
        <div class="product-images">
            <img src="/public/images/<?=$leProduit['image']?>" id="GrosseImage">
            <div class="product-miniatures">
                <div>
                    <img src="/public/images/<?=$leProduit['image']?>" class="miniature">
                </div>
                <div>
                    <img src="/public/images/<?=$leProduit['image_alt1']?>" class="miniature">
                </div>
                <div>
                    <img src="/public/images/<?=$leProduit['image_alt2']?>" class="miniature">
                </div>
                <div>
                    <img src="/public/images/<?=$leProduit['image_alt3']?>" class="miniature">
                </div>
            </div>
        </div>
        <div class="product-infos">
            <p class="product-category"><?=$leProduit['category']?></p>
            <h1><?=$leProduit['name']?></h1>
            <p class="product-price"><?=$leProduit['price']?>€</p>
            <form>
                <button type="button" id="reduire">-</button>
                <button type="button" disabled value=1 id="quantite">1</button>
                <button type="button" id="augmenter">+</button>
                <input type="submit" value="Ajouter au panier">
                <div class="box error" style="visibility: hidden" id="BoiteErreur">
                    Quantité maximale autorisée !
                </div>
            </form>
        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2>Spécificités</h2>
            <?=$leProduit['spec']?>
        </div>
        <div class="product-comments">
            <h2>Avis</h2>
            <?php foreach ($params["comment"] as $commentaire) :?>
                <div class="product-comment">
                    <p class="product-comment-author"><?php $commentaire['firstname']." ".$commentaire['lastname'] ?></p>
                    <p>
                        <?php $commentaire['content']  ?>
                    </p>
                </div>
            <?php endforeach ; ?>
                <form id="comment" method="post" action="/store/postComment">
                    <input type="hidden" name="identifiant" value=<?= $leProduit['id'] ?>>
                        <p>Il n'y a pas d'avis sur ce produit</p>
                    <input type="text" placeholder="Rédiger un commentaire" style="margin-top: 20px" name="commentaire">
                </form>
        </div>
    </div>
</div>

<script src="/public/scripts/product.js"></script>