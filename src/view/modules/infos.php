<div id="infos">
    <form class="info" method="post" action="/account/infos" id="infos">
        <h1>Informations du compte</h1>
        <h3>Informations personnelles</h3>

        <p id="currfname">Prénom</p>
        <input type="text" name="currfirstname" placeholder="<?= $_SESSION['firstname'] ?>" id="currfirstname"/>

        <p id="currlname">Nom</p>
        <input type="text" name="currlastname" placeholder="<?= $_SESSION['lastname'] ?>" id="currlastname"/>

        <p id="currmail">Adresse mail</p>
        <input type="text" name="currmail" placeholder="<?= $_SESSION['mail'] ?>" id="currmail"/>

        <input type="submit" value="Modifier mes informations" />

        <h3>Commandes</h3>
        <h4>Tu n'as pas de commandes en cours</h4>
    </form>
</div>