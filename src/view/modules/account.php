<?php if (isset($params['status']) && $params['status']=="login_fail") :?>
    <div class="box error" style="margin-left: 32px; margin-top: 30px" id="BoiteErreur">
        La connexion a échoué. Vérifiez vos identifiants et réessayer.
    </div>
<?php endif ; ?>
<?php if (isset($params['status']) && $params['status']=="signin_fail") :?>
    <div class="box error" style="margin-left: 32px; margin-top: 30px" id="BoiteErreur">
        L'inscription a échoué. Vérifiez que l'adresse mail n'est pas déjà utilisée.
    </div>
<?php endif ; ?>
<?php if (isset($params['status']) && $params['status']=="signing_success") :?>
    <div class="box success" style="margin-left: 32px; margin-top: 30px">
        Inscription réussie. Vous pouvez dès à présent vous connecter.
    </div>
<?php endif ; ?>
<?php if (isset($params['status']) && $params['status']=="logout") :?>
    <div class="box success" style="margin-left: 32px; margin-top: 30px">
        Vous êtes déconnecté. A bientôt!
    </div>
<?php endif ; ?>

<div id="account">

<form class="account-login" method="post" action="/account/login">

  <h2>Connexion</h2>
  <h3>Tu as déjà un compte ?</h3>

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin" id="account-signin">

  <h2>Inscription</h2>
  <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

  <p id="checklname">Nom</p>
  <input type="text" name="userlastname" placeholder="Nom" id="userlastname"/>

  <p id="checkfname">Prénom</p>
  <input type="text" name="userfirstname" placeholder="Prénom" id="userfirstname"/>

  <p id="checkmail">Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" id="usermail"/>

  <p id="check1">Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" id="userpass1"/>

  <p id="check2">Répéter le mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" id="userpass2"/>

  <input type="submit" value="Inscription" />

</form>

</div>

<script src="/public/scripts/signin.js"></script>