<nav>
    <img src="/public/images/logo.jpeg">
    <a href="/">Accueil</a>
    <a href="/store">Boutique</a>
    <?php if (isset($_SESSION['id'])) :?>
    <a class="account" href="/account">
        <a href="account/infos"><img src="/public/images/avatar.png"> <?= $_SESSION['firstname']." ".$_SESSION['lastname'] ?></a>
        <a href="/">Panier</a>
        <a href="/account/logout">Deconnexion</a>
    </a>
    <?php else : ?>
    <a class="account" href="/account">
        <img src="/public/images/avatar.png">
        Compte
    </a>
    <?php endif ; ?>
</nav>