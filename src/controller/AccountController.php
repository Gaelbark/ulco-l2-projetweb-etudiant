<?php


namespace controller;

use model\AccountModel;

class AccountController
{
    static function account(): void {
        // Variables à transmettre à la vue
        $params = array(
            "title" => "Account",
            "module" => "account.php",
            "status" => ($_GET['status'] ?? "")
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }
    static function login(): void {

        $stock=AccountModel::login($_POST["usermail"], $_POST["userpass"]);


        if (password_verify($_POST["userpass"],$stock['password']) && $stock['mail']==$_POST['usermail']){
            $_SESSION['id']=$stock['id'];
            $_SESSION['firstname']=$stock['firstname'];
            $_SESSION['lastname']=$stock['lastname'];
            $_SESSION['mail']=$stock['mail'];
            header("Location: /store");
            exit();
        }
        else{
            header("Location: /account?status=login_fail");
            exit();
        }

    }

    static function signin(): void {
        $stock=AccountModel::signin($_POST["userfirstname"], $_POST["userlastname"], $_POST["usermail"], $_POST["userpass"]);
        if ($stock){
            header("Location: /account?status=signing_success");
            exit();
        }
        else{
            header("Location: /account?status=signing_fail");
            exit();
        }
    }

    static function logout() : void{
        if (isset($_SESSION))
            session_destroy();
        header("Location: /account?status=logout");
        exit();
    }

    static function infos(){

    }
}