<?php

namespace controller;

use model\StoreModel;

class CommentController {

    public function postComment(): void
    {
        $comment = \model\CommentModel::insertComment($_POST["commentaire"], $_POST["identifiant"], $_SESSION["id"]);

        //if ($comment) {
            header("Location: /store/" . $_POST["identifiant"]); // on redirige vers /store/{num_produit}
            exit();
        //}
    }

}