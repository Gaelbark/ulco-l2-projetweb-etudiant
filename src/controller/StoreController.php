<?php

namespace controller;

class StoreController {

  static function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $product = \model\StoreModel::listProducts($_POST['category'] ?? null, $_POST['order'] ?? "none",
        $_POST['search'] ?? "");

    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
      "product"=> $product
    );

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }

  public static function product(int $id){
      $info=\model\StoreModel::infoProduct($id);
      $liste=\model\CommentModel::listComment($id);

      if (isset($info[0])){
          $params = array(
              "title" => "Product",
              "module" => "product.php",
              "info" => $info[0],
              "comment" => $liste
          );

          // Faire le rendu de la vue "src/view/Template.php"
          \view\Template::render($params);
      }
      else {
          header("Location: /store");
          exit();
      }
  }

  public static function search(){
      // Communications avec la base de données
      $categories = \model\StoreModel::listCategories();
      $product = \model\StoreModel::listProducts($_POST['category'] ?? null, $_POST['order'] ?? "none",
          $_POST['search'] ?? "");

      // Variables à transmettre à la vue
      $params = array(
          "title" => "Store",
          "module" => "store.php",
          "categories" => $categories,
          "product"=> $product
      );

      // Faire le rendu de la vue "src/view/Template.php"
      \view\Template::render($params);
  }

}