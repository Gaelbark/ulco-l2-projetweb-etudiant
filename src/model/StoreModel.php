<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function listProducts(array $categories = null, string $Ordre = "none", string $Filtre = "") {
      $cat="";
      $filter="";
      $orderby=" ORDER BY product.id ASC";
      if ($categories!=null){
          for ($i = 0; $i < count($categories); ++$i)
              $categories[$i] = "'" . $categories[$i] . "'";
          $cat=" WHERE category.name IN (".implode(',', $categories).") ";
      }
      if ($Filtre != ""){
          $filter=" WHERE product.name LIKE :name ";
      }
      if ($Ordre != "none"){
          $orderby=" ORDER BY product.price ".$Ordre;
      }
      // Connexion à la base de données
      $db = \model\Model::connect();

      //Requête SQL
      $sql = "SELECT product.id, product.name, product.price, product.image, product.spec, category.name 
        AS category FROM product INNER JOIN category
            ON product.category = category.id ".$filter.$cat.$orderby;
      $req = $db->prepare($sql);
      // Exécution de la requête
      if ($Filtre != ""){
          $req->execute(['name'=>'%'.htmlspecialchars($Filtre).'%']);
      }
      else{
          $req->execute();
      }
      // Retourner les résultats (type array)
      return $req->fetchAll();
  }

  static function infoProduct(int $id){
      // Connexion à la base de données
      $db = \model\Model::connect();

      //Requête SQL
      $sql = "SELECT product.id, product.name, product.price, product.image,
       product.image_alt1, product.image_alt2, product.image_alt3, product.spec,
       category.name AS category FROM product INNER JOIN category ON product.category = category.id
        WHERE product.id = :id";
      // Exécution de la requête
      $req = $db->prepare($sql);
      $req->execute([ "id" => $id]);
      // Retourner les résultats (type array)
      return $req->fetchAll();
  }
}