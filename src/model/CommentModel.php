<?php

namespace model;

class CommentModel {

    public static function insertComment($content, $id_product, $id_account)
    {
        $db = \model\Model::connect();
        $sql="INSERT INTO comment (content, date, id_product, id_account) VALUES (:content,
                                                                    NOW(), :id_product, :id_account)";
        $req = $db->prepare($sql);
        $req->execute(['content'=>$content, 'id_product'=>$id_product, 'id_account'=>$id_account ]);
        return $req->fetchAll();
    }

    public static function listComment($id_product) {
        $db = \model\Model::connect();
        $sql2="SELECT comment.content, account.firstname, account.lastname FROM comment INNER JOIN account ON comment.id_account = account.id WHERE comment.id_product = :id";
        $req = $db->prepare($sql2);
        $req->execute(['id'=>$id_product]);
        return $req->fetchAll();
    }

}