<?php


namespace model;


class AccountModel
{
    static function check($firstname, $lastname, $mail, $password) : bool{
        if (strlen($firstname)<2 || strlen($lastname)<2 || !filter_var($mail, FILTER_VALIDATE_EMAIL) ||strlen($password)<6){return false;}
        return true;
    }

    static function signin($firstname, $lastname, $mail, $password) : bool{
        if (!self::check($firstname, $lastname, $mail, $password)){
            return false;
        }
        $db = \model\Model::connect();

        $sql1="SELECT COUNT(*) AS number FROM account WHERE account.mail LIKE :mail";
        $req = $db->prepare($sql1);
        $req->execute([ "mail" => $mail]);

        $stock=$req->fetch();


        if ($stock["number"]>0){
            return false;
        }

        $sql2="INSERT INTO account (firstname, lastname, mail, password) VALUES (:firstname, :lastname, :mail, :password)";

        $req2 = $db->prepare($sql2);
        $req2->execute([ "firstname" => $firstname, "lastname" => $lastname, "mail" => $mail, "password" => password_hash($password, PASSWORD_ARGON2I)]);

        return true;
    }

    static function login($mail, $password){
        $db = \model\Model::connect();

        $sql="SELECT id, firstname, lastname, mail, password FROM account WHERE mail =:mail";

        $req = $db->prepare($sql);
        $req->execute(["mail" => $mail]);

        return $req->fetch();
    }
}