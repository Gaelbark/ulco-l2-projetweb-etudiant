<?php

session_start();
/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');

//STORE
$router->get('/store', 'controller\StoreController@store');

//STORE PRODUCT
$router->get('/store/{:num}', function($id) {controller\StoreController::product($id);});

//ACCOUNT
$router->get('/account', 'controller\AccountController@account');

//ACCOUNT
$router->post('/account/login', 'controller\AccountController@login');

//ACCOUNT
$router->get('/account/logout', 'controller\AccountController@logout');

//ACCOUNT
$router->post('/account/signin', 'controller\AccountController@signin');

//COMMENT
$router->post('/store/postComment', 'controller\CommentController@postComment');

//SEARCH
$router->post('/store/search', 'controller\StoreController@search');

//ACCOUNT
$router->get('/account/infos', 'controller\AccountController@infos');

// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
// Route "/" (racine)