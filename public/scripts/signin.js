const mailRegex = /\S+@\S+\.\S+/;
const numRegex = /[0-9]+/;
const alphaRegex = /[A-Za-z]+/;
let userfirstname= document.getElementById("userfirstname")
let userlastname= document.getElementById("userlastname")
let usermail= document.getElementById("usermail")
let userpass1= document.getElementById("userpass1")
let userpass2= document.getElementById("userpass2")
let checklname=document.getElementById("checklname")
let checkfname=document.getElementById("checkfname")
let checkmail=document.getElementById("checkmail")
let check1=document.getElementById("check1")
let check2=document.getElementById("check2")

document.getElementById("account-signin").onchange = function() {check()};
document.getElementById("account-signin").onsubmit= function (e) {
    if (!check()){
            e.preventDefault();
    }
}

function check(){
    let stock=true;
    if (userfirstname.value.length<2){
        stock=false;
        checkfname.classList.add("invalid");
        checkfname.classList.remove("valid");
    }
    else{
        checkfname.classList.remove("invalid");
        checkfname.classList.add("valid");
    }
    if (userlastname.value.length<2){
        stock=false;
        checklname.classList.add("invalid");
        checklname.classList.remove("valid");
    }
    else{
        checklname.classList.remove("invalid");
        checklname.classList.add("valid");
    }
    if (!mailRegex.test(usermail.value)){
        stock=false;
        checkmail.classList.add("invalid");
        checkmail.classList.remove("valid");
    }
    else{
        checkmail.classList.remove("invalid");
        checkmail.classList.add("valid");
    }
    if (userpass1.value.trim().length > 5 && userpass2.value.trim().length > 5
        && userpass1.value.localeCompare(userpass2.value) === 0
        && numRegex.test(userpass1.value.trim())
        && alphaRegex.test(userpass1.value.trim())){
        check1.classList.remove("invalid");
        check1.classList.add("valid");
        check2.classList.remove("invalid");
        check2.classList.add("valid");
    }
    else{
        stock=false;
        check1.classList.add("invalid");
        check1.classList.remove("valid");
        check2.classList.add("invalid");
        check2.classList.remove("valid");
    }

    return stock;
}