let miniature=document.getElementsByClassName("miniature")
let achats=document.getElementById("quantite")
let stock=1

for (let i=0; i<miniature.length; i++){
    miniature[i].addEventListener("click", function (){
        document.getElementById("GrosseImage").src=miniature[i].src
    });
}

document.getElementById("reduire").addEventListener("click", function (){
    if (stock<=1){
        return
    }
    stock-=1
    achats.value=stock
    achats.innerText=stock
    document.getElementById("BoiteErreur").style.visibility="hidden"
})

document.getElementById("augmenter").addEventListener("click", function (){
    if (stock>=5){
        document.getElementById("BoiteErreur").style.visibility="visible"
        return
    }
    stock+=1
    achats.value=stock
    achats.innerText=stock
})
